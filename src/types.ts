
import * as React from 'react'

export const META_SYMBOL = Symbol('mobforms meta')

export const RESERVED_FIELDS = [
  'constructor',
  'render',
  'componentWillMount',
  'componentDidMount',
  'componentWillUnmount',
  'componentDidUpdate',
  'componentDidCatch',
  'componentWillUpdate',
  'componentWillReceiveProps',
  'shouldComponentUpdate',
  'getSnapshotBeforeUpdate',
]

export type MobxFormMeta =
| {
  type: typeof String,
  name: string,
  defaultValue: string,
}
| {
  type: typeof Number,
  name: string,
  defaultValue: number,
}
| {
  type: [typeof String],
  name: string,
  defaultValue: string[],
}
| {
  type: [typeof Number],
  name: string,
  defaultValue: number[],
}

export type MobxFormType = typeof String | typeof Number | [typeof String] | [typeof Number]
export interface TypedFormMeta<T> {
  type: MobxFormType,
  name: string,
  defaultValue: T,
}

export interface FormFieldType<T = string> {
  readonly [META_SYMBOL]: TypedFormMeta<T>
}

export interface FieldComponentClass<P = {}, FF = string, FS = {}> extends React.ComponentClass<P> {
  new (props: P, context?: any): FieldComponent<P, FF, FS, React.ComponentState>
}

export abstract class FieldComponent<P = {}, FF = string, FS = {}, S = {}> extends React.Component<P, S> {
  get fieldValue () {
    const meta = (this.constructor as any as FormFieldType<FF>)[META_SYMBOL]
    return meta.defaultValue
  }
  set fieldValue (val: FF) {
    return
  }
  readonly formState: FS | undefined
  abstract render (): JSX.Element
}

export abstract class FormComponent<P = {}, FS = {}, S = {}> extends React.Component<P, S> {
  formState: FS | undefined = undefined
}

export interface FormComponentClass<P = {}, FS = {}> extends React.ComponentClass<P> {
  new (props: P, context?: any): FormComponent<P, FS, React.ComponentState>
}

export interface WrappableComponent<P = {}, S = {}> extends React.Component<P, S> {
  render (): JSX.Element
}

export interface WrappableComponentClass<P> extends React.ComponentClass<P> {
  new (props: P, context?: any): WrappableComponent<P, React.ComponentState>
}

export interface Dictionary<T> {
  [key: string]: T
}
