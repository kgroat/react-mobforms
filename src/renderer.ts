
import { observable, IObservableObject } from 'mobx'
import { create, ReactTestRendererTree } from 'react-test-renderer'

import { MobxFormMeta, Dictionary, META_SYMBOL } from './types'

const getMetaFromTree = (tree: ReactTestRendererTree | ReactTestRendererTree[] | null, meta: Dictionary<MobxFormMeta>) => {
  if (!tree) {
    return meta
  }

  if (!Array.isArray(tree)) {
    tree = [tree]
  }

  tree.forEach(node => {
    if (!node) {
      return
    }
    const type = node.type as any
    if (type && type[META_SYMBOL]) {
      const fieldMeta: MobxFormMeta = type[META_SYMBOL]
      const { name } = fieldMeta
      if (meta[name]) {
        console.warn(`Two fields with the name ${name} were added to the same form.  Defaulting to the first definition found.`)
      } else {
        meta[name] = fieldMeta
      }
    }
    getMetaFromTree(node.rendered, meta)
  })

  return meta
}

export default <T extends Dictionary<any>>(element: JSX.Element) => {
  const wrapper = create(element, { createNodeMock: el => el })
  const tree = wrapper.toTree()

  const meta = getMetaFromTree(tree, {})
  const state = {} as T
  Object.keys(meta).forEach(key => {
    state[key] = meta[key].defaultValue
  })

  const formState = observable(state)

  Object.defineProperty(formState, META_SYMBOL, {
    writable: false,
    value: meta,
    enumerable: false,
  })

  return formState
}
