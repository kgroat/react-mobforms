
import * as React from 'react'
import { observable, transaction } from 'mobx'
import { inject, observer } from 'mobx-react'

import { MobxFormType, MobxFormMeta, FieldComponent, FieldComponentClass, FormFieldType, WrappableComponentClass, META_SYMBOL, RESERVED_FIELDS } from '../types'

export interface FormFieldDecorator<TFieldType> {
  <P = {}, C extends FieldComponentClass<P, TFieldType> = FieldComponentClass<P, TFieldType, any>>(target: C): C & FormFieldType<TFieldType>
}

export interface FormFieldDecoratorBuilder {
  (name: string, type?: typeof String, defaultValue?: string): FormFieldDecorator<string>
  (name: string, type: typeof Number, defaultValue: number): FormFieldDecorator<number>
  (name: string, type: [typeof String], defaultValue: string[]): FormFieldDecorator<string[]>
  (name: string, type: [typeof Number], defaultValue: number[]): FormFieldDecorator<number[]>
}

function formField (name: string, type: MobxFormType = String, defaultValue: any = '') {
  return function <P, FF, C extends FieldComponentClass<P>>(target: C): C & WrappableComponentClass<P & { formState?: any }> {
    const meta = {
      name,
      type,
      defaultValue,
    } as MobxFormMeta

    const Component = observer(target)
    Object.defineProperty(Component, META_SYMBOL, {
      get () {
        return meta
      },
    })

    class FormField extends React.Component<P & { formState?: any }> {
      private get fieldValue (): FF {
        if (this.props.formState && this.props.formState[name] !== undefined) {
          return this.props.formState[name]
        }

        return defaultValue
      }

      private set fieldValue (val: FF) {
        if (this.props.formState) {
          this.props.formState[name] = val
          this.component.forceUpdate()
        }
      }

      private component: FieldComponent

      render () {
        return (
          <Component
            ref={this.setComponent}
            {...this.props}
          >
            {this.props.children}
          </Component>
        )
      }

      private setComponent = (el: any) => {
        this.component = el
        if (el) {
          Object.defineProperties(el, {
            fieldValue: {
              get: () => {
                return this.fieldValue
              },
              set: (val: FF) => {
                this.fieldValue = val
              },
            },
            formState: {
              get: () => {
                return this.props.formState
              },
            },
          })
          el.forceUpdate()
        }
      }
    }

    const existingKeys = Object.keys(FormField.prototype).concat(RESERVED_FIELDS)
    Object.keys(Component.prototype)
      .filter(key => !existingKeys.some(k => k === key))
      .forEach(key => {
        Object.defineProperty(FormField.prototype, key, {
          value: Component.prototype[key],
          writable: false,
        })
      })

    return inject('formState')(observer(FormField)) as any
  }
}

export default formField as FormFieldDecoratorBuilder
