
import * as React from 'react'
import { transaction } from 'mobx'
import { observer, Provider } from 'mobx-react'

import { FormComponentClass, FormComponent, MobxFormMeta, META_SYMBOL, RESERVED_FIELDS } from '../types'
import renderer from '../renderer'

function form<P, FS extends { [key: string]: any }, C extends FormComponentClass<P, FS>> (target: C): C {
  const Component = observer(target)

  class Form extends FormComponent<P, FS> {
    get formState (): FS {
      return this._formState
    }
    set formState (val: FS) {
      if (!val) {
        return
      }
      transaction(() => {
        Object.keys(this.formMeta).forEach(key => {
          this._formState[key] = val[key]
        })
      })
    }

    get formMeta (): { [P in keyof FS]: MobxFormMeta } {
      const formMeta = this._formState[META_SYMBOL]
      console.log('formMeta', formMeta)
      return formMeta
    }

    private readonly _formState: FS

    constructor (props: P, context?: any) {
      super(props, context)
      this._formState = renderer<FS>(this.render())
    }

    render () {
      return (
        <Provider formState={this.formState}>
          <Component ref={this.setForm} {...this.props}>
            {this.props.children}
          </Component>
        </Provider>
      )
    }

    private setForm = (form: FormComponent<P, FS, React.ComponentState>) => {
      Object.defineProperty(form, 'formState', {
        get: () => {
          return this.formState
        },
        set: (val: FS) => {
          this.formState = val
        },
      })
    }
  }

  const existingKeys = Object.keys(Form.prototype).concat(RESERVED_FIELDS)
  Object.keys(Component.prototype)
    .filter(key => !existingKeys.some(k => k === key))
    .forEach(key => {
      Object.defineProperty(Form.prototype, key, {
        value: Component.prototype[key],
        writable: false,
      })
    })

  return observer(Form) as any
}

export default form
