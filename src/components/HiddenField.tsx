
import * as React from 'react'

import { MobxFormMeta, META_SYMBOL } from '../types'

export default (props: MobxFormMeta) => {
  const Component = (() => null) as any
  Component[META_SYMBOL] = props

  return <Component />
}
