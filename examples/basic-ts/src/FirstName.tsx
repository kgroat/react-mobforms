
import * as React from 'react'
import { action } from 'mobx'
import { formField, FieldComponent } from 'react-mobforms'

export interface FirstNameField {
  firstName: string
}

@formField('firstName')
export default class FirstName extends FieldComponent {
  render () {
    return (
      <input
        placeholder='First Name'
        value={this.fieldValue}
        onChange={this.onChange}
      />
    )
  }

  @action private onChange = (ev: React.ChangeEvent<HTMLInputElement>) => {
    this.fieldValue = ev.currentTarget.value
  }
}
