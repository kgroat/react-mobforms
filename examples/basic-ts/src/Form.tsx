
import * as React from 'react'
import { form, FormComponent, HiddenField } from 'react-mobforms'

import FirstName, { FirstNameField } from './FirstName'
import LastName, { LastNameField } from './LastName'
import Age, { AgeField } from './Age'
import Hobbies, { HobbiesField } from './Hobbies'
import AddHobby, { AddHobbyField } from './AddHobby'

export type FormState =
  & FirstNameField
  & LastNameField
  & AgeField
  & HobbiesField
  & AddHobbyField
  & {
    allHobbies: string[],
  }

function unique<T> (array: T[]) {
  return array.filter((value, index) => {
    return array.indexOf(value) === index
  })
}

@form
export default class UserForm extends FormComponent<{}, FormState> {
  render () {
    return (
      <form onSubmit={this.onSubmit}>
        <div><button type='button' onClick={this.prefill}>Pre-fill</button></div>
        <HiddenField name='allHobbies' type={[String]} defaultValue={['coding', 'fishing', 'filmography']} />
        <div><FirstName /></div>
        <div><LastName /></div>
        <div><Age /></div>
        <div><Hobbies /></div>
        <div><AddHobby /></div>
        <div><button type='submit'>Submit</button></div>
      </form>
    )
  }

  private prefill = () => {
    if (this.formState) {
      const hobbies = [
        'chess',
        'latin literature',
      ]

      this.formState = {
        ...this.formState,
        firstName: 'James',
        lastName: 'Madison',
        age: 265,
        allHobbies: unique([
          ...this.formState.allHobbies,
          ...hobbies,
        ]),
        hobbies,
      }
    }
  }

  private onSubmit = (ev: React.FormEvent<HTMLFormElement>) => {
    ev.preventDefault()
    console.log(this.formState)
    alert(JSON.stringify(this.formState))
  }

}
