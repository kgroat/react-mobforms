
import * as React from 'react'
import { action } from 'mobx'
import { formField, FieldComponent } from 'react-mobforms'

import { FormState } from './Form'

export interface AddHobbyField {
  newHobby: string
}

@formField('newHobby')
export default class Hobbies extends FieldComponent<{}, string, FormState> {
  render () {
    return (
      <div>
        <input
          placeholder='New Hobby'
          value={this.fieldValue}
          onChange={this.onChange}
        />
        <button type='button' onClick={this.addHobby}>Add</button>
      </div>
    )
  }

  @action private onChange = (ev: React.ChangeEvent<HTMLInputElement>) => {
    this.fieldValue = ev.currentTarget.value
  }

  @action private addHobby = () => {
    const { allHobbies = [] } = this.formState ? this.formState : {}
    if (this.fieldValue) {
      if (allHobbies.some(h => h === this.fieldValue)) {
        return
      }
      allHobbies.push(this.fieldValue)
      this.fieldValue = ''
    }
  }
}
