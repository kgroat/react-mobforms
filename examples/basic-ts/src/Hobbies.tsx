
import * as React from 'react'
import { formField, FieldComponent } from 'react-mobforms'

import { FormState } from './Form'

export interface HobbiesField {
  hobbies: string[]
}

@formField('hobbies', [String], ['fishing'])
export default class Hobbies extends FieldComponent<{}, string[], FormState> {
  render () {
    const { allHobbies = [] } = this.formState ? this.formState : {}
    const chosenHobbies = this.fieldValue

    return (
      <div>
        <span>Hobbies:</span>
        {
          allHobbies.map(hobby => (
            <div key={hobby}>
              <label>
                <input
                  type='checkbox'
                  value={hobby}
                  checked={chosenHobbies.some(h => h === hobby)}
                  onChange={this.toggleHobby}
                />
                {hobby}
              </label>
            </div>
          ))
        }
      </div>
    )
  }

  private toggleHobby = (ev: React.ChangeEvent<HTMLInputElement>) => {
    const hobby = ev.currentTarget.value
    const chosenHobbies = this.fieldValue
    const index = chosenHobbies.indexOf(hobby)
    if (index < 0) {
      chosenHobbies.push(hobby)
    } else {
      chosenHobbies.splice(index, 1)
    }
  }
}
