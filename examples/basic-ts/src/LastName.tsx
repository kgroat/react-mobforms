
import * as React from 'react'
import { action } from 'mobx'
import { formField, FieldComponent } from 'react-mobforms'

export interface LastNameField {
  lastName: string
}

@formField('lastName', String, 'Doe')
export default class LastName extends FieldComponent {
  render () {
    return (
      <input
        placeholder='Last Name'
        value={this.fieldValue}
        onChange={this.onChange}
      />
    )
  }

  @action private onChange = (ev: React.ChangeEvent<HTMLInputElement>) => {
    this.fieldValue = ev.currentTarget.value
  }
}
