
const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const ENV_PROD = 'production'
const ENV_DEV = 'development'
const env = (process.env.NODE_ENV && process.env.NODE_ENV.toLowerCase()) || ENV_DEV

const __DEV__ = env !== ENV_PROD

module.exports = {
  entry: './src/index.jsx',
  output: {
    filename: 'index.js',
    path: path.join(__dirname, 'public'),
    publicPath: '/'
  },
  devServer: {
    port: 3000,
  },
  devtool: 'eval-source-map',
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loaders: [
          {
            loader: 'babel-loader',
          }
        ],
        exclude: /node_modules/,
      },
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(env),
      '__DEV__': JSON.stringify(__DEV__),
    }),
    new HtmlWebpackPlugin({
      title: 'react-mobforms example',
      template: path.join(__dirname, 'src/index.ejs')
    }),
  ]
}
