
import * as React from 'react'
import { action } from 'mobx'
import { form, FormComponent, HiddenField } from 'react-mobforms'

import FirstName from './FirstName'
import LastName from './LastName'
import Age from './Age'
import Hobbies from './Hobbies'
import AddHobby from './AddHobby'

function unique (array) {
  return array.filter((value, index) => {
    return array.indexOf(value) === index
  })
}

@form
export default class UserForm extends FormComponent {
  render () {
    return (
      <form onSubmit={this.onSubmit}>
        <div><button type='button' onClick={this.prefill}>Pre-fill</button></div>
        <HiddenField name='allHobbies' type={[String]} defaultValue={['coding', 'fishing', 'filmography']} />
        <div><FirstName /></div>
        <div><LastName /></div>
        <div><Age /></div>
        <div><Hobbies /></div>
        <div><AddHobby /></div>
        <div><button type='submit'>Submit</button></div>
      </form>
    )
  }

  onSubmit = () => {
    ev.preventDefault()
    console.log(this.formState)
    alert(JSON.stringify(this.formState))
  }

  @action prefill = () => {
    if (this.formState) {
      const hobbies = [
        'chess',
        'latin literature',
      ]

      this.formState = {
        ...this.formState,
        firstName: 'James',
        lastName: 'Madison',
        age: 265,
        allHobbies: unique([
          ...this.formState.allHobbies,
          ...hobbies,
        ]),
        hobbies,
      }
    }
  }
}
