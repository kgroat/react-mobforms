
import * as React from 'react'
import { action } from 'mobx'
import { formField, FieldComponent } from 'react-mobforms'

@formField('lastName', String, 'Doe')
export default class LastName extends FieldComponent {
  render () {
    return (
      <input
        placeholder='Last Name'
        value={this.fieldValue}
        onChange={ev => this.fieldValue = ev.currentTarget.value}
      />
    )
  }

  @action onChange = (ev) => {
    this.fieldValue = parseInt(ev.currentTarget.value, 10)
  }
}
