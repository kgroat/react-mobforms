
import * as React from 'react'
import { render } from 'react-dom'
import DevTools from 'mobx-react-devtools'

import Form from './Form'

render(<Form />, document.getElementById('app'))
if (__DEV__) {
  render(<DevTools />, document.getElementById('devtools'))
}
