
import * as React from 'react'
import { action } from 'mobx'
import { formField, FieldComponent } from 'react-mobforms'

@formField('newHobby')
export default class Hobbies extends FieldComponent {
  render () {
    return (
      <div>
        <input
          placeholder='New Hobby'
          value={this.fieldValue}
          onChange={this.onChange}
        />
        <button type='button' onClick={this.addHobby}>Add</button>
      </div>
    )
  }

  @action onChange = (ev) => {
    this.fieldValue = ev.currentTarget.value
  }

  @action addHobby = () => {
    const { allHobbies = [] } = this.formState ? this.formState : {}
    if (this.fieldValue) {
      if (allHobbies.some(h => h === this.fieldValue)) {
        return
      }
      allHobbies.push(this.fieldValue)
      this.fieldValue = ''
    }
  }
}
