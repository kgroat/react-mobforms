
import * as React from 'react'
import { action } from 'mobx'
import { formField, FieldComponent } from 'react-mobforms'

@formField('age', Number, 18)
export default class FirstName extends FieldComponent {
  render () {
    const inputValue = !Number.isNaN(this.fieldValue)
                     ? `${this.fieldValue}`
                     : ''
    return (
      <input
        type='number'
        placeholder='Age'
        value={inputValue}
        onChange={this.onChange}
      />
    )
  }

  @action onChange = (ev) => {
    this.fieldValue = parseInt(ev.currentTarget.value, 10)
  }
}
