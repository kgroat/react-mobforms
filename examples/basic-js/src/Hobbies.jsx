
import * as React from 'react'
import { action } from 'mobx'
import { formField, FieldComponent } from 'react-mobforms'

@formField('hobbies', [String], ['fishing'])
export default class Hobbies extends FieldComponent {
  render () {
    const { allHobbies = [] } = this.formState ? this.formState : {}
    const chosenHobbies = this.fieldValue

    return (
      <div>
        <span>Hobbies:</span>
        {
          allHobbies.map(hobby => (
            <div key={hobby}>
              <label>
                <input
                  type='checkbox'
                  value={hobby}
                  checked={chosenHobbies.some(h => h === hobby)}
                  onChange={this.toggleHobby}
                />
                {hobby}
              </label>
            </div>
          ))
        }
      </div>
    )
  }

  @action toggleHobby = (ev) => {
    const hobby = ev.currentTarget.value
    const index = chosenHobbies.indexOf(hobby)
    if (index < 0) {
      chosenHobbies.push(hobby)
    } else {
      chosenHobbies.splice(index, 1)
    }
  }
}
