
import * as React from 'react'
import { action } from 'mobx'
import { formField, FieldComponent } from 'react-mobforms'

@formField('firstName')
export default class FirstName extends FieldComponent {
  render () {
    return (
      <input
        placeholder='First Name'
        value={this.fieldValue}
        onChange={ev => this.fieldValue = ev.currentTarget.value}
      />
    )
  }

  @action onChange = (ev) => {
    this.fieldValue = parseInt(ev.currentTarget.value, 10)
  }
}
